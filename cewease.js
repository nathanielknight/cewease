
var t = (function(s) {

    function mkElement(etype, attrs, rawChilds) {
        let element = document.createElement(etype);
        console.log(`(mke ${etype} ${attrs} ${rawChilds}`);
        let keys = Object.keys(attrs);
        for (let i in keys) {
            let key = keys[i],
                val = attrs[key];
            element.setAttribute(key, val);
        }
        let childs = rawChilds.map(transform);
        return combine(element, childs);
    }
    

    function transform(x) {
        function tString(s) {
            return s;
        }

        function tNumber(x) {
            return x.toString();
        }

        function tArray (xs) {
            if (xs.length === 3) {
                let etype = xs[0],
                    attrs = xs[1],
                    childs = xs[2];
                return mkElement(etype, attrs, childs);
            } else if (xs.length === 2) {
                let etype = xs[0],
                    attrs,
                    childs;
                if (Array.isArray(xs[1])) {
                    childs = xs[1];
                    attrs = {};
                } else {
                    childs = [];
                    attrs = xs[1];
                }
                return mkElement(etype, attrs, childs);
            }
            throw `Invalid argument to tArray: ${xs}`;
        }

        if (typeof(x) === "string") {
            return tString(x);
        } 
        if (typeof(x) === "number") {
            return tNumber(x);
        }
        if (Array.isArray(x)) {
            return tArray(x);
        }

        throw `Invalid argument to transform: ${x}`;
    }
    

    function combine(parent, childs) {
        return childs.reduce(function (p, child) {
            if (typeof(child) === "string") {
                p.append(" ");
                p.append(child);
                return p
            } else {
                p.appendChild(child);
                return p
            }
        }, parent);
    }

    function render(x) {
        return transform(x);
    }
    return {
        render: render
    }
})();
